package project;

import java.sql.*;

public class Unit {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/musiclibrary?serverTimezone=UTC", "root", "root");
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM musiclibrary.composers");
            while (rs.next()){
                int composerId = rs.getInt("ComposersId");
                String firstName = rs.getString("FirstName");
                String lastName = rs.getString("LastName");
                System.out.println("Composers: " + " - " + composerId + " " + firstName + " " + lastName);
            }

            rs.close();
            stmt.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
