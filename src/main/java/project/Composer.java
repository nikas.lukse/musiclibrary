package project;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "composers")
public class Composer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    public int composerId;
    @Column
    public String firstName;
    @Column
    public String lastName;

    @ManyToOne
    @JoinColumn(name = "epochId")
    private Epoch epoch;

    @OneToMany(mappedBy = "title")
    private Set<Title> titles = new HashSet<Title>();

    public int getComposerId() {
        return composerId;
    }

    public void setComposerId(int composerId) {
        this.composerId = composerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    @Override
    public String toString() {

        return this.composerId+" | "+this.firstName+" | "+this.lastName;
    }
}
