package project;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "epochs")
public class Epoch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    public int epochsId;
    @Column
    public String epochName;

    @OneToMany(mappedBy = "composer")
    private Set<Composer> composers = new HashSet<Composer>();

    public int getEpochsId() {
        return epochsId;
    }

    public void setEpochsId(int epochsId) {
        this.epochsId = epochsId;
    }

    public String getEpochName() {
        return epochName;
    }

    public void setEpochName(String epochName) {
        this.epochName = epochName;
    }
}
