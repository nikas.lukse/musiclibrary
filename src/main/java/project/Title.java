package project;

import javax.persistence.*;

@Entity
@Table(name = "titles")
public class Title {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    public int composerId;
    @Column
    public String firstName;

    @ManyToOne
    @JoinColumn(name = "composerId")
    private Composer composer;

    @ManyToOne
    @JoinColumn(name = "epochId")
    private Epoch epoch;

    public int getComposerId() {
        return composerId;
    }

    public void setComposerId(int composerId) {
        this.composerId = composerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

}
